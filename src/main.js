import React from 'react'
import { render } from 'react-dom'

import App from './App'

const root = document.getElementById('root')

if (!root) {
  throw Error('Pososyamba')
}

render(<App title='Hello 🐓' />, root)
