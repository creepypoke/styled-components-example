import React from 'react'
import styled from 'astroturf'

const PageTitle = styled('h1')`
  @import '/theme/colors';
  @import '/theme/typography';

  @extend %size-1;

  color: $text;
`

export default React.memo(PageTitle)
