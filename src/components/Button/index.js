import React from 'react'
import styled from 'astroturf'

const Button = styled('button').attrs({
  type: 'button',
})`
  cursor: pointer;
  border: 1px solid red;
  background-color: lightpink;
  transition: background-color 0.25s linear;
  /* super unsupported property */
  user-select: none;

  &:hover {
    background-color: lightgreen;
  }

  @media (min-width: 960px) {
    display: block;
    font-size: 1.4rem;
  }

  &.custom-property-with-weird-name {
    box-shadow: 0 0.1rem 0.5rem 1px lightskyblue;
    transition: box-shadow, transform 0.25s ease-in-out;

    &:hover {
      box-shadow: 0 0 1px 1px lightskyblue;
    }

    &:active {
      transform: rotateX(360deg);
    }
  }

  &.isError {
    background-color: red;
    color: white;
  }
`

export default React.memo(Button)
