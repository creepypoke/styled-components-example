import React from 'react'

import { Button, PageTitle } from '/components'

import '/theme/base.scss'

const App = ({ children, title }) => {
  const [isError, setIsError] = React.useState(false)

  return (
    <>
      <PageTitle>{title}</PageTitle>
      {children}
      <Button
        custom-property-with-weird-name
        onClick={() => setIsError(!isError)}
        isError={isError}
      >
        Hello Darkness
      </Button>
      <Button as='div'>This is div</Button>
    </>
  )
}

export default React.memo(App)
